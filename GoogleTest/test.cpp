#include "maquina.c"
#include <gtest/gtest.h>

TEST(State0, Machine0){
    ASSERT_EQ(goNORTE, maquina(no_hay_carros,goNORTE));
}
TEST(State1, Machine1){
    ASSERT_EQ(goNORTE, maquina(hay_carros_norte,goNORTE));
}
TEST(State2, Machine2){
    ASSERT_EQ(waitNORTE, maquina(hay_carros_este,goNORTE));
}
TEST(State3, Machine3){
    ASSERT_EQ(waitNORTE, maquina(carros_ambos_lados,goNORTE));
}



TEST(State4, Machine4){
    ASSERT_EQ(goESTE, maquina(no_hay_carros,waitNORTE));
}
TEST(State5, Machine5){
    ASSERT_EQ(goESTE, maquina(hay_carros_este,waitNORTE));
}
TEST(State6, Machine6){
    ASSERT_EQ(goESTE, maquina(hay_carros_norte,waitNORTE));
}
TEST(State7, Machine7){
    ASSERT_EQ(goESTE, maquina(carros_ambos_lados,waitNORTE));
}



TEST(State8, Machine8){
        ASSERT_EQ(goESTE, maquina(no_hay_carros,goESTE));
}
TEST(State9, Machine9){
        ASSERT_EQ(goESTE, maquina(hay_carros_este,goESTE));    
}
TEST(State10, Machine10){
        ASSERT_EQ(waitESTE, maquina(hay_carros_norte,goESTE));
}
TEST(State11, Machine11){
        ASSERT_EQ(waitESTE, maquina(carros_ambos_lados,goESTE));
}



TEST(State12, Machine12){
        ASSERT_EQ(goNORTE, maquina(no_hay_carros,waitESTE));
}
TEST(State13, Machine13){
        ASSERT_EQ(goNORTE, maquina(hay_carros_este,waitESTE));
}
TEST(State14, Machine14){
        ASSERT_EQ(goNORTE, maquina(hay_carros_norte,waitESTE));
}
TEST(State15, Machine15){
        ASSERT_EQ(goNORTE, maquina(carros_ambos_lados,waitESTE));
}


int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
