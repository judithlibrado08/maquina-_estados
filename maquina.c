#include <stdio.h>

typedef enum{
    goNORTE,
    waitNORTE,
    goESTE,
    waitESTE
}
estados;

typedef enum{
	no_hay_carros,
	hay_carros_este,
	hay_carros_norte,
	carros_ambos_lados
}
estados2;


static int estado_actual = goNORTE;

void impestado_actual(int actual){
	
        if(actual ==goNORTE){
            printf("goNORTE");
            }
        if(actual == waitNORTE){
            printf("waitNORTE");
            }
        if(actual == goESTE){
            printf("goESTE");
            }
        if(actual == waitESTE){
            printf("waitESTE");
            }
	
};



int maquina(int estado2, int estado)
{

	if (estado == goNORTE){
            if (estado2 == no_hay_carros || estado2 == hay_carros_norte){
		        estado_actual = goNORTE;
            }else{
		        estado_actual = waitNORTE;
            }
        }
    
    if (estado == waitNORTE){
	        estado_actual = goESTE;
        }

    if (estado == goESTE){
            if (estado2 == no_hay_carros || estado2 == hay_carros_este){
		        estado_actual = goESTE;
            }else{
		        estado_actual = waitESTE;
            }
        }
    
    if (estado == waitESTE){
	        estado_actual = goNORTE;
        
    }
		
	 impestado_actual(estado_actual);
};


int main(){
    int Transiciones[8] ={
        hay_carros_este, 
	no_hay_carros,
        hay_carros_este, 
        hay_carros_norte, 
        carros_ambos_lados,
	hay_carros_norte,
        no_hay_carros,
        carros_ambos_lados,
        
    };	

    printf("\nEstado al momento [");
    impestado_actual(estado_actual);
    printf("]");    

    for (int i = 0; i < 8; i++){
	    printf("\nEstado al momento [");
        maquina(Transiciones[i], estado_actual);
        printf("]");
    }
    printf("\n\n");
    return 0;    
}
